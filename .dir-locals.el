((c-mode . ((c-file-style . "k&r")
            (tab-width . 2)
            (c-basic-offset . 2)
            (flycheck-gcc-include-path . ("include"))))

 (ld-script-mode . ((tab-width . 2))))
