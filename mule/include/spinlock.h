/*
 * Copyright 2020 Raghu Kaippully.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#ifndef SPINLOCK_H
#define SPINLOCK_H

#include <stdint.h>


typedef uint8_t spinlock_t;

static inline void spin_lock(spinlock_t *l)
{
  uint8_t unlocked = 0, locked = 1;
  asm volatile("1:                   "
               "lock                ;"
               "cmpxchg   %2, %0    ;"
               "jz        2f        ;"
               "pause               ;"
               "jmp       1b        ;"
               "2:                   "
               : "+m"(*l), "+a"(unlocked)
               : "r"(locked));
}

static inline void spin_unlock(spinlock_t *l)
{
  asm volatile("movb   $1, %0" : "=m"(*l));
}

#endif
