/*
 * Copyright 2020 Raghu Kaippully.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include <stdint.h>
#include <attribs.h>
#include <kprelude.h>
#include <x86.h>

gdt_entry_t gdt[] ALIGN(8) = {
  // NULL descriptor, selector = 0x00
  {0, 0, 0, 0, 0, 0},

  // kernel code segment, selector = 0x08
  {
    .limit_low    = 0xffff,
    .base_low     = 0,
    .base_mid     = 0,
    .attribs_low  = 0b10011011,  // code, present, dpl = 0, execute/read, accessed
    .attribs_high = 0b10101111,  // long mode, limit (mid)
    .base_high    = 0
  },

  // kernel data segment, selector = 0x10
  {
    .limit_low    = 0xffff,
    .base_low     = 0,
    .base_mid     = 0,
    .attribs_low  = 0b10010011,  // data, present, dpl = 0, r/w, accessed
    .attribs_high = 0b11001111,  // 4k granular, limit (mid)
    .base_high    = 0
  },

  // user data segment, selector = 0x18
  {
    .limit_low    = 0xffff,
    .base_low     = 0,
    .base_mid     = 0,
    .attribs_low  = 0b11110011,  // data, present, dpl = 3, r/w, accessed
    .attribs_high = 0b11001111,  // 4k granular, limit (mid)
    .base_high    = 0
  },

  // user code segment, selector = 0x20
  {
    .limit_low    = 0xffff,
    .base_low     = 0,
    .base_mid     = 0,
    .attribs_low  = 0b11111011,  // code, present, dpl = 3, execute/read, accessed
    .attribs_high = 0b10101111,  // long mode, limit (mid)
    .base_high    = 0
  },

  /*
   * TSS descriptor, selector = 0x28.
   *
   * TSS is located at 0xffff8000001fd000 in the virtual address space of all
   * processes. Its size is 8297 bytes (104 bytes for the basic TSS + 8193 bytes for
   * I/O permission bit map)
   *
   * In long mode, TSS occupies two entries in GDT. The limit_low and base_low fields
   * of the higher entry holds base[63:32] instead and all other fields are set to zero.
   */
  {
    .limit_low    = 8297,
    .base_low     = 0xd000,
    .base_mid     = 0x1f,
    .attribs_low  = 0b10001001,  // tss, present, dpl = 0
    .attribs_high = 0b00000000,  // byte granular, limit (mid)
    .base_high    = 0
  },
  {
    .limit_low    = 0x8000,      // base[32:47]
    .base_low     = 0xffff,      // base[48:63]
    .base_mid     = 0,
    .attribs_low  = 0,
    .attribs_high = 0,
    .base_high    = 0
  }
};

descriptor_t gdtr_descriptor = {
  .limit = sizeof(gdt) - 1,
  .base  = gdt
};

kprelude_t kprelude SECTION(".prelude") = {
  .gdtr_descriptor = &gdtr_descriptor
};
