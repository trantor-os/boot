/*
 * Copyright 2020 Raghu Kaippully.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include <stdint.h>
#include <attribs.h>
#include <spinlock.h>

typedef enum {
  READY, RUNNING, BLOCKED, TERMINATED
} process_state_t;

typedef struct {
  // Area saved and restored by fxsave, fxrstor instructions
  // Must be 16-byte aligned
  uint8_t fxsave[512];

  // Basic registers
  uint64_t rax, rbx, rcx, rdx, rdi, rsi, rbp, rsp;
  uint64_t r8, r9, r10, r11, r12, r13, r14, r15;
  uint64_t rflags;
} PACKED ALIGN(16) regs_t;

typedef uint32_t pid_t;

typedef struct process_t {
  pid_t pid, ppid;

  process_state_t state;

  // A linked list of all processes
  struct process_t *next;

  // A linked list of ready processes
  struct process_t *next_ready;

  // State of the process
  uint64_t pml4;
  regs_t regs;
} process_t;


static process_t *all_procs = 0;

static process_t *ready_procs = 0;

static volatile process_t *running_proc = 0;
