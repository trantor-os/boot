/*
 * Copyright 2020 Raghu Kaippully.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#ifndef X86_H
#define X86_H

#include <stdint.h>
#include <attribs.h>

static inline void sti()
{
  asm volatile("sti");
}

static inline void cli()
{
  asm volatile("cli");
}

static inline void hlt()
{
  asm volatile("hlt");
}

static inline uint8_t inb(uint16_t port)
{
  uint8_t ret;
  asm volatile("inb %1, %0" : "=a"(ret) : "Nd"(port));
  return ret;
}

static inline void outb(uint16_t port, uint8_t val)
{
  asm volatile("outb %0, %1" : : "a"(val), "Nd"(port));
}

typedef struct {
  uint16_t limit_low;
  uint16_t base_low;
  uint8_t  base_mid;
  uint8_t  attribs_low;
  uint8_t  attribs_high;
  uint8_t  base_high;
} PACKED gdt_entry_t;

typedef struct {
  uint16_t limit;
  void     *base;
} PACKED descriptor_t;

static inline void lgdt(descriptor_t *m)
{
  asm volatile("lgdt %0" : : "m"(*m));
}

static inline void lidt(descriptor_t *m)
{
  asm volatile("lidt %0" : : "m"(*m));
}

#endif
