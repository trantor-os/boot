/*
 * Copyright 2020 Raghu Kaippully.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#ifndef STRING_H
#define STRING_H

static inline int isdigit(char c)
{
  return c >= '0' && c <= '9';
}

extern int strlen(const char* s);

#endif
