/*
 * Copyright 2020 Raghu Kaippully.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#ifndef KPRELUDE_H
#define KPRELUDE_H

/* A datastructure in kernel used by earth for initializing it. This
 * is necessary because mule is a flat binary with no headers/symbol
 * tables.
 */
typedef struct {
  void* gdtr_descriptor;
  void* idtr_descriptor;
  void* syscall_handler;
} kprelude_t;

#endif
