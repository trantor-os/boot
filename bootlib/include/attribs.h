/*
 * Copyright 2020 Raghu Kaippully.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#ifndef ATTRIBS_H
#define ATTRIBS_H

#define PACKED     __attribute__((packed))
#define ALIGN(x)   __attribute__((aligned(x)))
#define SECTION(s) __attribute__((section(s)))
#define INTERRUPT  __attribute__((interrupt))
#define NORETURN   __attribute__((noreturn))

#endif
