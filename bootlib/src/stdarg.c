/*
 * Copyright 2020 Raghu Kaippully.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include <stdarg.h>
#include <string.h>
#include <stdint.h>
#include <x86.h>


/* A limited version of vsnprintf that is not standards compliant.
 *
 * Supports format strings of the form:
 *    %[flags][width][length]type
 * where
 *    flags  - one of '-', '+', '0'
 *    width  - the minimum number of characters to output
 *    length - one of 'l', 'll'
 *    type   - one of '%', 'd', 'u', 'x', 's', 'c', 'p'
 */
void vsnprintf(char *buf, unsigned n, const char *fmt, va_list args)
{
  int state = 0;

  bool left_align = false, with_sign = false, zero_pad = false;
  unsigned min_width = 0;
  enum {
    len_none, len_long, len_long_long
  } length = len_none;


  unsigned print_char(unsigned idx)
  {
    char c = va_arg(args, int);
    buf[idx] = c;
    return idx+1;
  }

  unsigned print_padded_string(unsigned idx, const char *s, unsigned len)
  {
    unsigned pad = min_width > len ? min_width - len : 0;

    if (!left_align) {
      for (; pad > 0 && idx < n - 1; pad--, idx++)
        buf[idx] = ' ';
    }
    while (idx < n - 1 && *s)
      buf[idx++] = *s++;
    if (left_align) {
      for (; pad > 0 && idx < n - 1; pad--, idx++)
        buf[idx] = ' ';
    }

    return idx;
  }

  unsigned print_string(unsigned idx)
  {
    char *s = va_arg(args, char*);
    unsigned len = strlen(s);

    return print_padded_string(idx, s, len);
  }

  unsigned print_int(unsigned idx)
  {
    long long num = 0;
    switch (length) {
    case len_none: num = va_arg(args, int); break;
    case len_long: num = va_arg(args, long); break;
    case len_long_long: num = va_arg(args, long long); break;
    }

    char digits[21];
    const char dec_chars[] = "0123456789";
    int i;
    digits[20] = '\0';
    for (i = 19; i >= 0 && num != 0; i--) {
      digits[i] = dec_chars[num % 10];
      num /= 10;
    }
    if (with_sign || num < 0)
      digits[i--] = num < 0 ? '-' : '+';

    return print_padded_string(idx, &digits[i+1], 19 - i);
  }

  unsigned print_unsigned(unsigned idx)
  {
    unsigned long long num = 0;
    switch (length) {
    case len_none: num = va_arg(args, unsigned int); break;
    case len_long: num = va_arg(args, unsigned long); break;
    case len_long_long: num = va_arg(args, unsigned long long); break;
    }

    char digits[21];
    const char dec_chars[] = "0123456789";
    int i;
    digits[20] = '\0';
    for (i = 19; i >= 0 && num != 0; i--) {
      digits[i] = dec_chars[num % 10];
      num /= 10;
    }
    if (with_sign)
      digits[i--] = '+';

    return print_padded_string(idx, &digits[i+1], 19 - i);
  }

  unsigned print_hex_num(unsigned idx, unsigned long long num)
  {
    char digits[17];
    const char hex_chars[] = "0123456789abcdef";
    int i;
    digits[16] = '\0';
    for (i = 15; i >= 0 && num != 0; i--) {
      digits[i] = hex_chars[num % 16];
      num /= 16;
    }

    return print_padded_string(idx, &digits[i+1], 15 - i);
  }

  unsigned print_hex(unsigned idx)
  {
    unsigned long long num = 0;

    switch (length) {
    case len_none: num = va_arg(args, unsigned int); break;
    case len_long: num = va_arg(args, unsigned long); break;
    case len_long_long: num = va_arg(args, unsigned long long); break;
    }

    return print_hex_num(idx, num);
  }

  unsigned print_pointer(unsigned idx)
  {
    uint64_t num = va_arg(args, uint64_t);
    return print_hex_num(idx, num);
  }



  // State machine
  unsigned idx;
  for (idx = 0; *fmt && idx < n - 1;) {
    switch (state) {
    case 0:  // Normal state
      if (*fmt == '%')
        state = 1;
      else
        buf[idx++] = *fmt;
      fmt++;
      break;

    case 1:  // Flags
      if (*fmt == '-') {
        left_align = true;
        fmt++;
      } else if (*fmt == '+') {
        with_sign = true;
        fmt++;
      } else if (*fmt == '0') {
        zero_pad = true;
        fmt++;
      } else {
        left_align = with_sign = zero_pad = false;
        state = 2;
      }
      break;

    case 2:  // Width
      min_width = 0;
      while (isdigit(*fmt))
        min_width = min_width*10 + *fmt++ - '0';
      state = 3;
      break;

    case 3:  // Length
      if (*fmt == 'l') {
        fmt++;
        if (*fmt == 'l') {
          length = len_long_long;
          fmt++;
        } else {
          length = len_long;
        }
      } else {
        length = len_none;
      }
      state = 4;
      break;

    case 4:  // Type
      switch (*fmt++) {
      case '%':
        buf[idx++] = '%';
        break;
      case 'd':
        idx = print_int(idx);
        break;
      case 'u':
        idx = print_unsigned(idx);
        break;
      case 'x':
        idx = print_hex(idx);
        break;
      case 's':
        idx = print_string(idx);
        break;
      case 'c':
        idx = print_char(idx);
        break;
      case 'p':
        idx = print_pointer(idx);
        break;
      }
      state = 0;
      break;
    }
  }
  buf[idx] = '\0';
}
