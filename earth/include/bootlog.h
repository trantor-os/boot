/*
 * Copyright 2020 Raghu Kaippully.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#ifndef BOOTLOG_H
#define BOOTLOG_H

#include <stdint.h>

typedef enum {
  BL_DEBUG,
  BL_INFO,
  BL_WARN,
  BL_ERROR
} loglevel_t;

#if defined(BOOTLOG_DEBUG)
#define log_debug(fmt, ...) boot_log(BL_DEBUG, fmt, ##__VA_ARGS__)
#else
#define log_debug(fmt, ...)
#endif

#if defined(BOOTLOG_DEBUG) || defined(BOOTLOG_INFO)
#define log_info(fmt, ...) boot_log(BL_INFO, fmt, ##__VA_ARGS__)
#else
#define log_info(fmt, ...)
#endif

#if defined(BOOTLOG_DEBUG) || defined(BOOTLOG_INFO) || defined(BOOTLOG_WARN)
#define log_warn(fmt, ...) boot_log(BL_WARN, fmt, ##__VA_ARGS__)
#else
#define log_warn(fmt, ...)
#endif

#if !defined(BOOTLOG_NONE)
#define log_error(fmt, ...) boot_log(BL_ERROR, fmt, ##__VA_ARGS__)
#else
#define log_error(fmt, ...)
#endif

extern void boot_log(loglevel_t lvl, const char *fmt, ...);

#endif
