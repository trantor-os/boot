/*
 * Copyright 2020 Raghu Kaippully.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include <bootlog.h>
#include <attribs.h>
#include <x86.h>
#include <stdarg.h>


typedef struct {
  loglevel_t level;
  uint16_t   size;
  // The message begins here and is `size` bytes long (including
  // the trailing null char)
  uint8_t    msg;
} PACKED logentry_t;


/* VGA console handling
 *
 * These assume that we have access to the video memory at 0xb8000 and
 * that we can write to VGA ports to move the cursor.
 */
static uint8_t con_y = 0;

static void move_cursor()
{
  uint16_t pos = con_y*80;

  outb(0x3d4, 0x0f);
  outb(0x3d5, pos & 0xff);
  outb(0x3d4, 0x0e);
  outb(0x3d5, (pos >> 8) & 0xff);
}

#define VBASE ((uint16_t*)0xb8000)

static void scroll_up(int n)
{
  uint16_t *p = VBASE, *q = VBASE + n*80;
  while(q < VBASE + 80*25)
    *p++ = *q++;
  con_y -= n;
}

static void console_write(logentry_t *e)
{
  int len = e->size - 1, lines = (len + 79)/80;
  if (con_y + lines > 25)
    scroll_up(lines);

  uint16_t attr = 0x0700;

  switch (e->level) {
  case BL_DEBUG: attr = 0x0800; break;  // gray
  case BL_INFO:  attr = 0x0700; break;  // white
  case BL_WARN:  attr = 0x0600; break;  // brown
  case BL_ERROR: attr = 0x0400; break;  // red
  }

  uint8_t *p = &e->msg;
  uint16_t *vmem = VBASE + con_y*80;
  for (; *p && len >= 0; len--)
    *vmem++ = attr | *p++;
  con_y += lines;

  move_cursor();
}


/* Boot logs are capped at this size */
#define LOG_SIZE 8192

static uint8_t boot_logs[LOG_SIZE];

static logentry_t *next_log = (logentry_t*)boot_logs;

void boot_log(loglevel_t lvl, const char *fmt, ...)
{
  char msg[256];

  va_list args;
  va_start(args, fmt);
  vsnprintf(msg, 256, fmt, args);
  va_end(args);

  if (next_log >= (logentry_t*)(boot_logs + LOG_SIZE))
    return;

  // Create a logentry_t
  next_log->level = lvl;
  char *msg_start = (char*)&next_log->msg, *p = msg_start;
  for (int i = 0; msg[i] && i < 256; i++) {
    *p++ = msg[i];
  }
  *p++ = '\0';
  next_log->size = p - msg_start;

  console_write(next_log);

  next_log = (logentry_t*)p;
}
