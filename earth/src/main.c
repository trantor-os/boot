/*
 * Copyright 2020 Raghu Kaippully.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include <x86.h>
#include <multiboot.h>
#include <string.h>
#include <bootlog.h>


extern void hlt_forever();

static void panic(const char* msg)
{
  log_error(msg);
  hlt_forever();
}

static void load_boot_components()
{
  char *module_names[4];

  /* Split the command line args on space characters. The first
   * argument is mule. The rest are boot modules. */
  char *p = mb_cmdline;
  int state = 0;
  for (int i = 0; *p && i < 4; p++) {
    if (state == 0) {
      module_names[i++] = p;
      state = 1;
    } else if (state == 1) {
      if (*p == ' ') {
        *p = 0;
        state = 0;
      }
    }
  }

  if (module_names[0] == 0)
    panic("Missing kernel name in command line.");
  else if (module_names[1] == 0 || module_names[2] == 0 || module_names[3] == 0)
    panic("Missing boot modules in command line.");

  log_debug("kernel = %s", module_names[0]);
  for (int i = 1; i < 4; i++)
    log_debug("boot_module[%d] = %s", i, module_names[i]);
}

void kmain()
{
  log_info("Booting Trantor OS.");
  load_boot_components();
}
